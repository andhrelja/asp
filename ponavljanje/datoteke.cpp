#include <iostream>
#include <fstream>
#include <string>

std::fstream datoteka;

// Pisanje u datoteku
// ios::out == "w"
// ios::in == "r"
// ios::app == "a"

int main() {

    datoteka.open("test.txt", std::ios::out);

    if (datoteka.is_open()) {
        for (int i=0; i < 5; i++) {
            datoteka << i << std::endl;
        }
        datoteka.close();
    }

    datoteka.open("test.txt", std::ios::in);

    if (datoteka.is_open()) {
        std::string redak;
        while (getline(datoteka, redak)) {
            for (int i=0; i < (int)redak.length(); i++) {
                int broj = redak[i];
                std::cout << ((int)broj) - ((int)'0') << std::endl; // <=> cout << broj - '0' << endl;
            }
        }
        datoteka.close();
    }
}

