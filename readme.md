# **Algoritmi i strukture podataka** #
Odjel za informatiku, Sveučilište u Rijeci
Ak. god. **2019./2020.**
  
Nositelj kolegija: prof. dr. sc. **Maja Matetić**  
Asistent: mag. inf. **Milan Petrović**  
Demonstrator: **Andrea Hrelja**

## **Sadržaj**

### **demonstrature/**  

```
Materijali obrađeni na demonstraturama:
    - Binarna Stabla
    - Pretraživanje grafova - Širina i Dubina (Breadth & Depth)
```

### **ponavljanje/**  

```
Datoteke i Rekurzija
```
