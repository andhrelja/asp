#include <iostream>
#include <vector>

using namespace std;


void stvoriGraf(vector<pair<int, int>> adj[], int nVertex)
{
	for (int i = 0; i < nVertex; i++)
	{ //0,1
		for (int j = 0; j < nVertex; j++)
		{
			int create_edge = rand() % 2;
			if (create_edge == 1 && i != j)
			{
				int wt = 1 + (rand() % 20);
				adj[i].push_back(make_pair(j, wt));
				//adj[j].push_back(make_pair(i, wt));
			}
		}
	}
}

void mst(vector<pair<int, int>> adj[], int nVertex)
{
	std::vector<int> pathMem(nVertex);
	for (int k = 0; k < nVertex; ++k) {
		int udaljenost = 1000;
		vector<int> bio(nVertex, 0);     // bio[10] = {0,0,0..., 0}
		vector<int> dist(nVertex, 1000); // dist[10] = {1000,1000,1000..., 1000}
		dist[k] = 0; //slucajno odabrani pocetni cvor
		// dist[] = {0,1000,1000..., 1000}
		int MST = 0;
		for (int i = 0; i < nVertex; i++)
		{ //Primov algoritam
			for (int j = 0; j < nVertex; j++) {
				if (!bio[j] && dist[j] < udaljenost)
				{
					int tko = j;
					int udaljenost = dist[j];
					bio[tko] = 1;
					MST += udaljenost;
				}
			}
			for (auto it = adj[i].begin(); it != adj[i].end(); it++)
			{
				if (dist[i] > it->second) {
					//cout << i << "-->" << it->first << " tezina: " << it->second << endl;
				}
				dist[i] = it->second;
			}
		}
		//cout << "Put sa najmaljim udaljenostima: " << MST << endl;
		pathMem[k] = MST;
	}

	int max = pathMem[0];
	int sum = pathMem[0];
	for (int i = 1; i < nVertex; ++i) {
		if (pathMem[i] > max) {
			max = pathMem[i];
		}
		sum += pathMem[i];
	}

	std::cout << "Put s najvecom najmanjom udaljenosti: " << max << '\n';
	std::cout << "Prosjek: " << (float)sum / nVertex;
}

int main()
{
	const int V = 10;
	vector<pair<int, int>> adj[V];

	stvoriGraf(adj, V);
	mst(adj, V);

	//  1. Dopunite funkciju mst() tako da ona prolazi kroz graf za sve cvorove, 
	//      a ne samo za jedan zadani kao sto je dano u primjeru

    //  2. Pohranite duljinu puta za svaki cvor u grafu i ispiste cvor koji sadrzi put sa najvecom
	//      /najmanjom udaljenosti i koliko ona iznosi
    
    //  3. Ispisite prosjek za sve cvorove u grafu

	return 0;
}