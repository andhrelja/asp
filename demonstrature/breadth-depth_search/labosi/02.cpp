#include <iostream>
#include<list>
#include<vector>

using namespace std;

#define nVertex 50

void stvoriGraf(list<int> a[])
{
    for(int i = 0; i < nVertex; i++){
        for(int j = 0; j < nVertex; j++){
            int create_edge = rand() % 25;
            if(create_edge == 1 && i != j)
                a[i].push_back(j);
        }
    }
}

int pretraziGraf(list<int> a[]) {
    vector<int> n_a;

    for(int i = 0; i<nVertex; i++) {
        int count = 0;
        for(auto it = a[i].begin(); it != a[i].end(); ++it) {
            //cout << *it << ", ";
            count++;
        }

        if(count == 0)
            n_a.push_back(count);
    }

    return n_a.size();
}

void pozovi(int i) {
    list<int> a[nVertex];

    cout << i << ". poziv: ";
    stvoriGraf(a);
    
    int broj_praznih = pretraziGraf(a); 
    cout << "broj cvorova koji nemaju niti jednu vezu: " << broj_praznih << endl;
    cout << endl;
}

int main()
{
    //Potrebno je 10 puta pozvati funkciju stvoriGraf() zatim u stvorenom grafu pronaci cvorove koji nemaju niti jednu vezu i prebrojati koliko takvih cvorova ima.
    //Za pronalazenje cvorova koji nemaju niti jednu vezu koristite funkciju pretraziGraf(). Pohranite broj cvorova za svaku od 10 iteracija i zatim ispisite prosjek.

    for (int i=1; i<11; i++) {
        pozovi(i);
    }

    return 0;
}
