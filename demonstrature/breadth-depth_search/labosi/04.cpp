#include <iostream>
#include<list>
#include<array>

#define nVertex 50

using namespace std;

void stvoriGraf(list<int> a[])
{
    for (int i = 0; i < nVertex; i++) {
        for (int j = 0; j < nVertex; j++) {
            int create_edge = rand() % 25;
            if (create_edge == 1 && i != j)
                a[i].push_back(j);
        }
    }
}

int prebrojVeze(list<int> a[])
{
    int suma = 0;
    for (int i = 0; i < nVertex; ++i) 
        suma += a[i].size();
    return suma;
}

int prebrojVeze5(list<int> a[])
{
    int suma = 0;
    for (int i = 0; i < nVertex; ++i) 
        suma += (a[i].size() == 5);
    return suma;
}

void pozovi(int &sumaSve, int &suma5) {
    list<int> a[nVertex];

    stvoriGraf(a);
    sumaSve += prebrojVeze(a);
    suma5 += prebrojVeze5(a);
}


int main()
{
    int sumaSvihVeza = 0;
    int suma5Veza = 0;
    float n = 20;

    for (int i = 0; i < n; ++i) {
        pozovi(sumaSvihVeza, suma5Veza);
    }

    cout << "Prosjecan broj veza za 20 grafova: " << sumaSvihVeza / n << endl;
    cout << "Prosjecan broj cvorova sa 5 veza za 20 grafova: " << suma5Veza / n << endl;

    return 0;
}