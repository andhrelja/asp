#include <limits.h> // za koristenje INT_MAX
#include <iostream>
#include <vector>

using namespace std;

#define V 9 // Definicija konstante V

// Pomocna funkcija za pronalazenje vrha sa najmanjom vrijednosti udaljenosti
// od skupa vrhova koji jos nisu ukljuceni u najkraci put
int minDistance(int dist[], bool sptSet[])
{
    // inicijalizacija minimalne vrijednosti
    int min = INT_MAX, min_index;

    for (int v = 0; v < 9; v++)
        if (sptSet[v] == false && dist[v] <= min)
            min = dist[v], min_index = v;

    return min_index;
}

// Pomocna funkcija za ispis polja sa pohranjenim udaljenostima
void printSolution(int dist[])
{
    cout << "Vertex \t\t Distance from Source" << endl;
    for (int i = 0; i < V; i++)
        cout << i << "\t\t" << dist[i] << endl;
}

void dijkstra(int graph[V][V], int src)
{
    int dist[V];
    bool sptSet[V];
    
    for (int i = 0; i < V; i++)
        dist[i] = INT_MAX, sptSet[i] = false;
    dist[src] = 0;

    for (int count = 0; count < V - 1; count++)
    {
        int u = minDistance(dist, sptSet); // Definirana pomocna funkcija
        sptSet[u] = true;

        for (int v = 0; v < V; v++)
            if (sptSet[v] == false 
                && graph[u][v] != 0 
                && dist[u] != INT_MAX 
                && dist[u] + graph[u][v] < dist[v])
                    dist[v] = dist[u] + graph[u][v];
    }
    // Ispis najkracih putova
    printSolution(dist);
}

int main()
{
    int graph[V][V] = {
        {0, 4,  0, 0,  0,  0,  0, 8,  0},
        {4, 0,  8, 0,  0,  0,  0, 11, 0},
        {0, 8,  0, 7,  0,  4,  0, 0,  0},
        {0, 0,  7, 0,  9,  14, 0, 0,  0},
        {0, 0,  0, 9,  0,  10, 0, 0,  0},
        {0, 0,  4, 14, 10, 0,  2, 0,  0},
        {0, 0,  0, 0,  0,  2,  0, 1,  0},
        {8, 11, 0, 0,  0,  0,  1, 0,  0},
        {0, 0,  0, 0,  0,  0,  0, 0,  0}};

    dijkstra(graph, 0);

    return 0;
}