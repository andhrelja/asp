#include <iostream>
#include <vector>

using namespace std;

void ispisiGraf(vector<pair<int, int>> adj[], int V)
{
    int v, w;
    for (int u = 0; u < V; u++)
    {
        cout << "Node " << u << " makes an edge with \n";
        for (auto it = adj[u].begin(); it != adj[u].end(); it++)
        {
            v = it->first;
            w = it->second;
            cout << "\tNode " << v << " with edge weight =" << w << "\n";
        }
        cout << "\n";
    }
}

void stvoriGraf(vector<pair<int, int>> adj[], int nVertex)
{
    for (int i = 0; i < nVertex; i++)
    { //0,1

        for (int j = 0; j < nVertex; j++)
        {
            int create_edge = rand() % 2;
            if (create_edge == 1 && i != j)
            {
                int wt = 1 + (rand() % 20);
                adj[i].push_back(make_pair(j, wt)); //adj[0] = {(3,5)}
                //adj[j].push_back(make_pair(i, wt));
            }
        }
    }
}

void mst(vector<pair<int, int>> adj[], int nVertex)
{
    int udaljenost = 1000;
    vector<int> bio(nVertex, 0);     // bio[10] = {0,0,0..., 0}
    vector<int> dist(nVertex, 1000); // dist[10] = {1000,1000,1000..., 1000}

    dist[0] = 0; //slučajno odabrani početni čvor
    // dist[] = {0,1000,1000..., 1000}
    int MST = 0;
    for (int i = 0; i < nVertex; i++)
    { //Primov algoritam
        for (int j = 0; j < nVertex; j++) {
            if (!bio[j] && dist[j] < udaljenost)
            {
                int tko = j;
                int udaljenost = dist[j];
                bio[tko] = 1;
                MST += udaljenost;
            }
        }
        for (auto it = adj[i].begin(); it != adj[i].end(); it++)
        {
            if (dist[i] > it->second) {
                cout << i << "-->" << it->first << " tezina: " << it->second << endl;
            }
            dist[i] = it->second;
        }
    }
    cout << "Put sa najmaljim udaljenostima: " << MST << endl;
}

int main()
{
    const int V = 10;
    vector<pair<int, int>> adj[V]; // adj[0] = (1, 7)

    stvoriGraf(adj, V);
    ispisiGraf(adj, V);
    mst(adj, V);

    return 0;
}