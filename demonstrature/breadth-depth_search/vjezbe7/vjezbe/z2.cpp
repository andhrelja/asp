#include <limits.h> // za koristenje INT_MAX
#include <iostream>
#include <vector>

using namespace std;

// Pomocna funkcija za pronalazenje vrha sa najmanjom vrijednosti udaljenosti
int minDistance(int dist[], bool sptSet[])
{
    return min_index;
}

// Pomocna funkcija za ispis polja sa pohranjenim udaljenostima
void printSolution(int dist[])
{

}

void dijkstra(int graph[V][V], int src)
{
    int dist[V];
    bool sptSet[V];

    // Trazimo najkraci put do svih vrhova
    for (int count = 0; count < V - 1; count++)
    {
        // Odaberemo vrh sa minimalnom udaljenosti od skupa vrhova koji još
        // nisu obrađeni. U prvoj iteraciji u je uvijek jednak src.

        ? minDistance(dist, sptSet); // Definirana pomocna funkcija
        
        // Oznacujemo odabrani vrh sa true

        // Azuriramo dist vrijednost susjednih vrhova za odabrani vrh.
        for (int v = 0; v < V; v++)
            // Azuriramo dist[v] samo ako nije u sptSet, postoji rub od u do v,
            // i ukupna težina putanje od src do v je manja od trenutne vrijednosti dist[v]
            if (!sptSet[v] && graph[u][v] && dist[u] != INT_MAX && dist[u] + graph[u][v] < dist[v])
                ?
    }
    // Ispis najkracih putova
    printSolution(dist);
}

int main()
{
    int graph[V][V] = {
        {0, 4, 0, 0, 0, 0, 0, 8, 0},
        {4, 0, 8, 0, 0, 0, 0, 11, 0},
        {0, 8, 0, 7, 0, 4, 0, 0, 2},
        {0, 0, 7, 0, 9, 14, 0, 0, 0},
        {0, 0, 0, 9, 0, 10, 0, 0, 0},
        {0, 0, 4, 14, 10, 0, 2, 0, 0},
        {0, 0, 0, 0, 0, 2, 0, 1, 6},
        {8, 11, 0, 0, 0, 0, 1, 0, 7},
        {0, 0, 2, 0, 0, 0, 6, 7, 0}
    };

    dijkstra(graph, 0);

    return 0;
}