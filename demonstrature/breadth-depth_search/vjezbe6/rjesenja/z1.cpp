#include <iostream>
#include <list>
#include <iterator>

using namespace std;

void dodaj(list<int> arr[], int i, int j);
void ispis(list<int> arr[], int n);

int main() {
    const int V = 12;
    list<int> lista[V];

    dodaj(lista, 0, 1);
    dodaj(lista, 0, 4);
    dodaj(lista, 1, 2);
    dodaj(lista, 1, 5);
    dodaj(lista, 1, 6);
    dodaj(lista, 2, 3);
    dodaj(lista, 2, 5);
    dodaj(lista, 2, 6);
    dodaj(lista, 3, 7);
    dodaj(lista, 4, 5);
    dodaj(lista, 4, 8);
    dodaj(lista, 5, 6);
    dodaj(lista, 5, 9);
    dodaj(lista, 6, 7);
    dodaj(lista, 6, 10);
    dodaj(lista, 7, 11);
    dodaj(lista, 8, 9);
    dodaj(lista, 9, 10);
    dodaj(lista, 10, 11);

    ispis(lista, V);

    return 0;
}

void dodaj(list<int> arr[], int i, int j)
{
    arr[i].push_back(j);
    arr[j].push_back(i);
}

void ispis(list<int> arr[], int n) 
{
    for(int i = 0; i < n; i++) {
        cout << "lista[" << i << "]:" << '\t';
        
        for(auto it = arr[i].begin(); it != arr[i].end(); ++it)
            cout << *it << '\t';
        
        cout << endl;
    }
}


/*
    int lista[12][12] = {
        {0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
        {1, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0},
        {0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0},
        {0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0},
        {1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0},
        {0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0},
        {0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0},
        {0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1},
        {0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0},
        {0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0},
        {0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1},
        {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0},
    };
*/