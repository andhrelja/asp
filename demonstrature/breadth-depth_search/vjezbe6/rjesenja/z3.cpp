#include <iostream>
#include <list>
#include <iterator>

#define V 8

using namespace std;

void dodaj(list<int> arr[], int i, int j);
void ispis(list<int> arr[]);
void DFS(list<int> arr[], int s);

int main() {
    list<int> lista[V];

    dodaj(lista, 0, 1);
    dodaj(lista, 0, 2);
    dodaj(lista, 0, 3);
    dodaj(lista, 1, 4);
    dodaj(lista, 1, 5);
    dodaj(lista, 3, 6);
    dodaj(lista, 3, 7);

    /*

    dodaj(lista, 0, 1);
    dodaj(lista, 0, 4);
    dodaj(lista, 1, 2);
    dodaj(lista, 1, 5);
    dodaj(lista, 1, 6);
    dodaj(lista, 2, 3);
    dodaj(lista, 2, 5);
    dodaj(lista, 2, 6);
    dodaj(lista, 3, 7);
    dodaj(lista, 4, 5);
    dodaj(lista, 4, 8);
    dodaj(lista, 5, 6);
    dodaj(lista, 5, 9);
    dodaj(lista, 6, 7);
    dodaj(lista, 6, 10);
    dodaj(lista, 7, 11);
    dodaj(lista, 8, 9);
    dodaj(lista, 9, 10);
    dodaj(lista, 10, 11);
    */

    ispis(lista);

    DFS(lista, 0);

    return 0;
}

void dodaj(list<int> arr[], int i, int j)
{
    arr[i].push_back(j);
    arr[j].push_back(i);
}

void ispis(list<int> arr[]) 
{
    for(int i = 0; i < V; i++) {
        cout << "lista[" << i << "]:" << '\t';

        for(auto it = arr[i].begin(); it != arr[i].end(); ++it) 
            cout << *it << '\t';
        cout << endl;
    }
}

void DFS(list<int> arr[], int s) 
{
    bool visited[8];
    for(int i = 0; i < 8; i++) 
        visited[i] = false;
    
    list<int> stack;
    stack.push_back(s);

    while(!stack.empty()) {
        s = stack.back();
        stack.pop_back();

        if (!visited[s]) {
            cout << s << '\t';
            visited[s] = true;

            for(auto it = arr[s].begin(); it != arr[s].end(); ++it)
                stack.push_back(*it);
        }
    }
}