/*** Zadatak 2: **
 - Nadopunite prethodni zadatak tako da napišete funkciju za pretragu binarnog stabla.
   Ona vraća zadanu vrijednost nako je pohranjena u čvoru danog stabla.
*/

#include <iostream>
#include <iomanip>

using namespace std;

struct Node
{
    int data;
    Node *left, *right;

    Node(int data)
    {
        this->data = data;
        left = right = NULL;
    }
};

void addNode(Node *&root, int data);
int findNode(Node *root, int data);

int main()
{
    int array[] = {17, 23, 19, 12, 14, 9, 72, 54, 76, 67};
    Node *root = new Node(50);

    for (int i = 0; i < 10; i++)
        addNode(root, array[i]);

    int search = 10;
    int found = findNode(root, search);

    if (found == 0) {
        cout << "Čvor nije pronađen" << endl;
    }
    else {
        cout << "Čvor (" << search << ") pronađen " << endl;
    }

    return 0;
}

void addNode(Node *&root, int data)
{
    Node *node = root;
    Node *newNode = new Node(data);

    if (data < node->data)
    {
        if (node->left == NULL)
            node->left = newNode;
        else
            addNode(node->left, data);
    }
    else if (data > node->data)
    {
        if (node->right == NULL)
            node->right = newNode;
        else
            addNode(node->right, data);
    }
}

int findNode(Node *root, int data) {
    if (root == NULL)
        return 0;
    else if (root->data == data)
        return root->data;
    else if (data < root->data) 
        return findNode(root->left, data);
    else
        return findNode(root->right, data);
}
