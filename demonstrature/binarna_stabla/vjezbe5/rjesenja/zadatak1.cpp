/*** Zadatak 1: **
 - Napišite program za stvoriti binarno stablo dano u nastavku:
  
             50
           /    \
         17      72
        /  \     / \
      12    23  54  76
     /  \   /     \
    9   14 19      67

*/

#include <iostream>
#include <iomanip>

using namespace std;

struct Node
{
    int data;
    Node *left, *right;

    Node(int data)
    {
        this->data = data;
        left = right = NULL;
    }
};

void addNode(Node *&root, int data);
void printTree(Node *root, int width);

int main()
{
    int array[] = {17, 23, 19, 12, 14, 9, 72, 54, 76, 67};
    Node *root = new Node(50);

    for (int i = 0; i < 10; i++)
        addNode(root, array[i]);
    printTree(root, 0);

    return 0;
}

void addNode(Node *&root, int data)
{
    Node *node = root;
    Node *newNode = new Node(data);

    if (data < node->data)
    {
        if (node->left == NULL)
            node->left = newNode;
        else
            addNode(node->left, data);
    }
    else if (data > node->data)
    {
        if (node->right == NULL)
            node->right = newNode;
        else
            addNode(node->right, data);
    }
}

void printTree(Node *root, int width)
{
    if (root == NULL) {
        cout << setw(width) << "x" << endl;
        return;
    }
    printTree(root->right, width + 7);
    cout << setw(width) << root->data << " < " << endl;
    printTree(root->left, width + 7);
}
