# Vježbe 5: Binarna stabla 2
  
## Zadatak 1.
Napišite program za stvoriti binarno stablo dano u nastavku:
  
             50
           /    \
         17      72
        /  \     / \
      12    23  54  76
     /  \   /     \
    9   14 19      67
  
## Zadatak 2.
Nadopunite prethodni zadatak tako da napišete funkciju za pretragu binarnog stabla. Ona vraća vrijednost ako je pohranjena u čvoru danog stabla.
