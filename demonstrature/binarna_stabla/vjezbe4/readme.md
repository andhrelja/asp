# Vježbe 4: Binarna stabla 1
  
## Zadatak 1.
Kreirajte proizvoljno binarno stablo, zatim definirajte funkciju koja vraća visinu stabla.
  

## Zadatak 2.
Nadopunite prethodni zadatak tako da za zadani čvor `n` funkcija vraća podstablo zadanog čvora.  
Dakle vaš zadatak je za zadano binarno stablo pronaći podstablo u zadanom čvoru `n` i njegovu visinu.
