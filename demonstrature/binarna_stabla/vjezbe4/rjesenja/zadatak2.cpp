/*** Zadatak 2: **
- Nadopunite prethodni zadatak tako da za zadani čvor `n` funkcija vraća podstablo zadanog čvora.  
  Dakle vaš zadatak je za zadano binarno stablo pronaći podstablo u zadanom čvoru `n` i njegovu visinu.
*/

#include <iostream>

using namespace std;

struct Node
{
    int data;
    Node *left, *right;

    Node(int data)
    {
        this->data = data;
        left = right = NULL;
    }
};

void addNode(Node *&root, int data);
int findNode(Node *root, int data);

int main()
{
    int array[] = {3, 2, 1, 5, 6, 7};
    Node *root = new Node(4);

    for (int i = 0; i < 6; i++)
        addNode(root, array[i]);

    int n = 5;
    int foundNode = findNode(root, n);
    
    if (foundNode == 0)
        cout << "Čvor nije pronađen" << endl;
    else
        cout << "Visina čvora (" << n << "): " << foundNode << endl;

    return 0;
}

void addNode(Node *&root, int data)
{
    Node *node = root;
    Node *newNode = new Node(data);

    if (data < node->data)
    {
        if (node->left == NULL)
            node->left = newNode;
        else
            addNode(node->left, data);
    }
    else if (data > node->data)
    {
        if (node->right == NULL)
            node->right = newNode;
        else
            addNode(node->right, data);
    }
}

int findNode(Node *root, int data) {
    if (root == NULL)
        return 0;
    else if (root->data == data)
        return root->data;
    else if (data < root->data) 
        return findNode(root->left, data);
    else
        return findNode(root->right, data);
}
