/*** Zadatak 1: **
 - Kreirajte proizvoljno binarno stablo, zatim definirajte funkciju koja vraća visinu stabla.
*/

#include <iostream>
#include <iomanip>

using namespace std;

struct Node
{
    int data;
    Node *left, *right;

    Node(int data)
    {
        this->data = data;
        left = right = NULL;
    }
};

void addNode(Node *&root, int data);
void printTree(Node *root, int width);
int treeHeight(struct Node *);

int main()
{
    int array[] = {3, 2, 1, 5, 6, 7};
    Node *root = new Node(4);

    for (int i = 0; i < 6; i++)
        addNode(root, array[i]);

    cout << "Binary Tree: " << endl;
    printTree(root, 0);

    cout << "Recursive calculated height: " << treeHeight(root) << endl;

    return 0;
}

void addNode(Node *&root, int data)
{
    Node *node = root;
    Node *newNode = new Node(data);

    if (data < node->data)
    {
        if (node->left == NULL)
            node->left = newNode;
        else
            addNode(node->left, data);
    }
    else if (data > node->data)
    {
        if (node->right == NULL)
            node->right = newNode;
        else
            addNode(node->right, data);
    }
}

void printTree(Node *root, int width)
{
    if (root == NULL)
    {
        cout << setw(width) << "x" << endl;
        return;
    }
    printTree(root->right, width + 7);
    cout << setw(width) << root->data << " < " << endl;
    printTree(root->left, width + 7);
}

int treeHeight(struct Node *node)
{
    if (node == NULL)
        return 0;
    int leftHeight = treeHeight(node->left);
    int rightHeight = treeHeight(node->right);

    if (leftHeight > rightHeight)
        return 1 + leftHeight;
    else if (leftHeight < rightHeight)
        return 1 + rightHeight;
}
